// console.log("Hello World");


//[section] Objects

/*

    - An object is a data type that is used to represent real world objects.
    - It is a collection of related data and/or functionalities/method. 
    - Information stored in objects are represented in a key:value pair
    - key is also mostly referred to as a "property" of an object.
//     - Different data types may be stored in an object's property createing data structures.

// */

let grades = {

    firstName : 'John',
    middleName : '',
    lastName: ''
    
}

//Creating objects using object initializers/ literal notation

/*
//Object literals.
    syntax:
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
    - This creates/declares an object and also intializes/ assign it's properties upon creation.
    - A Cellphone is an example of real world object.
    - it has its own properties such as name, color, weight, unit model and lot of other properties.
*/

let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999,
}

console.log(`Result from creating using literal notation: `);
console.log(cellphone);

//Creating objects using constructor function

/*
    -creates a reusable function to create several objects that have the same data structure.
    - This is useful for creating multiple instances/copies of an object.
    -An instance is a concrete occurrence of any object which emphasize distinct/unique identity of it.
    -syntax :
        function objectName(valueA, valueB){
            this.keyA = value A,
            this.keyB = valueB
        }
*/
    //constructor function always uppercase
    function Laptop(name, manufactureDate,ram){
        this.laptopName = name;
        this.laptopManufactureDate = manufactureDate;
        this.laptopRam = ram;
    }

    //instantiation.
        // The "new" operator creates an instance of an object.
        // Objects and instances are often interchange because object literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/ unique objects.


    let laptop1 = new Laptop('Toshiba', 'November 2007');

    console.log(laptop1);
    //using dot notation.
    console.log(laptop1.laptopName);

    let myLaptop = new Laptop("MacBook Air", 2020,'8gb');
    console.log("Result from creating objects using constructor function: ");
    console.log(myLaptop.laptopName, myLaptop.laptopManufactureDate, myLaptop.laptopRam);


    let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500 mb");
        /*
            The example above invoke/calls the laptop function instead of creating a new object.
            return "undefined" without the "new" operator because the "laptop" function does not have any return statement.
        */

    console.log("Result from creating objects without the new keyword: ");
    console.log(oldLaptop);


    // Mini activity
    // Create a contructor function that will let us instantiate a new object, menu, property : menuName, menuPrice


    function Menu(name, price){
        this.name = name,
        this.price = price
        
    }


    let menu1 = new Menu('Meal Set 1', 5000);
    console.log(menu1);
    let menu2 = new Menu('Meal Set 2', 3000);
    console.log(menu2);



	// function Menu(daName , daPrice){
	// 	this.menuName = daName;
	// 	this.menuName = daPrice;
	// }

	let mealOne = new Menu("Breakfast", 299);
	console.log(mealOne);

	// creating empty objects
	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

	// Accessing obj inside an array

	// let array = [laptop, myLaptop];
	// console.log(array.laptop);
	// console.log(array[0]);

	// /* Dot notation */
	// console.log(array[0].laptopManufactureDate);

	// console.log(laptop);
	// console.log(laptop.laptopName);

// [Section] Initializing/adding/deleting/reassigning Object properties.

	/*
		Syntax:
			like any other variable in JS, objects have their properties initialized/ added after the obj was created.
	*/

	let car = {};
	console.log(car);

	// Initializing object propertires using dot notation

	car.name = "Honda Civic";
	console.log(car);

	// Initializing obj property using bracket notation

	car['manufactureDate'] = 2019;

	console.log(car);

	// deleting obj properties
	delete car['name'];
	console.log(car);
	delete car.manufactureDate;
	console.log(car);

	// reassingning obj properties

		// using dot notation
		car.name = "Dodge Charger R/T";
		console.log(car);

		// using bracket notation
		car['name'] = "Jeepney";
		console.log(car);

// [Section] Object Methods
	// Moethod is a function w/c is a property of an object.
	// they are also functions and one of the key differences they have is that methods are functions realted to a specific objects.

	let person = {
		name: 'Chris',
		talk: function(){
			console.log("Hello my name is " + this.name);
		}
	}

	console.log(person);
	person.talk();

	// add method to objects
	person.walk = function(){
		console.log(this.name + " Walked 25 steps forward.")
	};

	person.walk();

	// methods are useful for creating reusable functions that perform tasks related to objects.
	let friends = {
		firstName: 'Jow',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			country: 'Texas',
		},
		phoneNumber: [['09123445643'],['091277846680']],
		emails: ['joe@gmail.com', 'joeSmith@email.xyz'],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + ' ' + this.lastName + '. I live in ' + this.address.city + ' ' + this.address.country + ". My emails are " + this.emails[0] + " and " + this.emails[1] + ". My numbers are " + this.phoneNumber[0][0] + ' and ' + this.phoneNumber[1][0])
		}
	}

	friends.introduce();

	/* create an obj constructor */

	function Pokemon(name, level){
		// Pokemon Method
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		/* method */

		this.tackle = function(target){
			console.log(this.pokemonName + ' tackles ' + target.pokemonName);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
		}
		this.fainted = function(){
			console.log(this.pokemonName + " Fainted!")
		}
	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados);
	pikachu.tackle(gyarados);

	gyarados.fainted();


